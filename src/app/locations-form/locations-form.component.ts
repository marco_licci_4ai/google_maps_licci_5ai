import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-locations-form',
  templateUrl: './locations-form.component.html',
  styleUrls: ['./locations-form.component.css']
})
export class LocationsFormComponent implements OnInit {

  @Output() onGo = new EventEmitter<string[]>();

  locationNames: string[];

  constructor() { }

  ngOnInit() {
    this.locationNames = Array(1);
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  addLocation(){
    this.locationNames.push("");
  }

  go(){
    this.onGo.emit(this.locationNames.filter(s => s.length > 0));
  }
}
