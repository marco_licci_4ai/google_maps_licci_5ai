import { Component, OnInit } from '@angular/core';

declare const google: any;

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit  {
    constructor() {}

    directionsService: any;
    directionsDisplay: any;
    mapProp = {
        center: new google.maps.LatLng(51.508742, -0.120850),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map: any;

    ngOnInit() {
      this.directionsService = new google.maps.DirectionsService;
      this.directionsDisplay = new google.maps.DirectionsRenderer;
      this.map = new google.maps.Map(document.getElementById("map"), this.mapProp);
      this.directionsDisplay.setMap(this.map);
    }

    go(locations: string[]){
      this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay, locations);
    }

    calculateAndDisplayRoute(directionsService, directionsDisplay, locations) {
      var waypts = [];

      for (let i = 1; i < locations.length - 1; i++) {
        waypts.push({
          location: locations[i],
          stopover: true
       });
      }
      
      console.log(waypts);

      directionsService.route({
        origin: locations[0],
        destination: locations[locations.length - 1],
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      }, function(response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
    }
}